package com.myApp.todo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.myApp.todo.R;
import com.myApp.todo.app.AppConfig;
import com.myApp.todo.app.AppController;
import com.myApp.todo.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class LoginActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputEmail = findViewById(R.id.email);
        inputPassword = findViewById(R.id.password);
        Button btnLogin = findViewById(R.id.btnLogin);
        Button btnLinkToRegister = findViewById(R.id.btnLinkToRegisterScreen);

        
        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);


        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {
                    // login user
                    checkLogin(email, password);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    private void checkPass(final String email, final String hash){
        String tag_string_req = "req_pass";
        StringRequest strReq2 = new StringRequest(Request.Method.GET,
                (AppConfig.URL+"?select=check_auth(a,b)&a="+email+"&b="+hash), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    boolean error;
                    JSONArray jArr = new JSONArray(response);
                    if(jArr.length() != 0) {
                        JSONObject jObj = jArr.getJSONObject(0);
                        if(jObj.getString("check_auth")!="null"){

                            if (jObj.has("error")) {
                                error = jObj.getBoolean("error");
                            } else {
                                error = false;
                            }
                            // Check for error node in json
                            if (!error) {

                                session.setLogin(true);
                                JSONObject jObj2 = jObj.getJSONArray("check_auth").getJSONObject(0);
                                // Now store the user in SQLite

                                String name = jObj2.getString("name");
                                String email = jObj2.getString("email");
                                String hash = jObj2.getString("hash");
                                String uid = jObj2.getString("id");


                                // Launch main activity
                                Intent intent = new Intent(LoginActivity.this,
                                        MainActivity.class);
                                intent.putExtra("com.myApp.todo.idString",uid);
                                intent.putExtra("com.myApp.todo.nameString",name);
                                intent.putExtra("com.myApp.todo.emailString",email);
                                intent.putExtra("com.myApp.todo.hashString",hash);
                                startActivity(intent);
                                finish();
                            } else {
                                // Error in login. Get the error message
                                String errorMsg = jObj.getString("error_msg");
                                Toast.makeText(getApplicationContext(),
                                        errorMsg, Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext(),"Parool on vale", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(getApplicationContext(),"Probleem kontrollimisel, kas andmebaas töötab?", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(strReq2, tag_string_req);
        // user successfully logged in
        // Create login session

    }

    private void getSalt(final String email, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                (AppConfig.URL+"?select=getsalt(a)&a="+email), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideDialog();
                try {
                    boolean error;
                    JSONArray jArr = new JSONArray(response);
                    JSONObject jObj = jArr.getJSONObject(0);
                    if (jObj.has("error")) {
                        error = jObj.getBoolean("error");
                    }else{
                        error =false;
                    }
                    // Check for error node in json
                    if (!error) {
                        String saltString = jObj.getString("getsalt");
                        saltString=saltString.replaceFirst("\\[","");
                        saltString=saltString.replaceFirst("]","");
                        saltString=saltString.replaceAll(" ","");
                        String[] preSalt = saltString.split(",");
                        String hash;
                        byte[] salt = new byte[16];
                        for (int i=0; i<preSalt.length; i++) {
                            salt[i]= (byte) parseInt(preSalt[i]);
                        }

                        try {
                            MessageDigest md = MessageDigest.getInstance("SHA-256");
                            md.update(salt);
                            //Loon hashi
                            byte[] bytes = md.digest(password.getBytes());
                            StringBuilder sb = new StringBuilder();
                            for (byte aByte : bytes) {
                                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
                            }
                            hash = sb.toString();
                            checkPass(email,hash);
                        }
                        catch (NoSuchAlgorithmException e)
                        {
                            e.printStackTrace();
                        }

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("getHash", "true");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void checkLogin(final String email, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.GET,
                (AppConfig.URL+"?select=usercheck(a)&a="+email), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    boolean error;
                    JSONArray jArr = new JSONArray(response);
                    JSONObject jObj = jArr.getJSONObject(0);
                    if (jObj.has("error")) {
                        error = jObj.getBoolean("error");
                    }else{
                        error =false;
                    }
                    // Check for error node in json
                    if (!error) {

                        if(jObj.getBoolean("usercheck")){
                            getSalt(email,password);
                        }
                        else{
                            hideDialog();
                            Toast.makeText(getApplicationContext(),
                                    "Sellist kasutajat ei ole registeeritud", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("getHash", "true");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}