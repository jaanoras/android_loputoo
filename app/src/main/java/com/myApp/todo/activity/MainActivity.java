package com.myApp.todo.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.myApp.todo.R;
import com.myApp.todo.app.AppConfig;
import com.myApp.todo.app.AppController;
import com.myApp.todo.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.widget.LinearLayout.HORIZONTAL;
import static com.android.volley.VolleyLog.TAG;
import static java.lang.Integer.parseInt;

public class MainActivity extends Activity {

    private EditText addThemeText;
    private SessionManager session;
    private LinearLayout themesLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        themesLayout = findViewById(R.id.themesLayout);
        TextView txtName = findViewById(R.id.name);
        addThemeText = findViewById(R.id.addThemeText);
        Button btnLogout = findViewById(R.id.btnLogout);
        Button btnAddTheme = findViewById(R.id.btnAddTheme);

        Intent intent = getIntent();
        final String uid = intent.getStringExtra("com.myApp.todo.idString");
        String name = intent.getStringExtra("com.myApp.todo.nameString");
        final String email = intent.getStringExtra("com.myApp.todo.emailString");
        final String hash = intent.getStringExtra("com.myApp.todo.hashString");
        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn() || email==null || hash==null || email.isEmpty() || hash.isEmpty()) {
            logoutUser();
        }

        getThemes(uid,email,hash);

        // Displaying the user details on the screen
        txtName.setText(name);

        // Logout button click event
        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
        btnAddTheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                addTheme(uid,email,hash);
            }
        });
    }

    private void logoutUser() {
        session.setLogin(false);


        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    private void addTheme(String uid, String email, String hash){
        String themeName = addThemeText.getText().toString().trim();
        if(!themeName.isEmpty()){
            checkIfThemeExists(themeName, uid, email, hash);
        }else{
            Toast.makeText(getApplicationContext(),
                    "Palun sisesta teemanimi", Toast.LENGTH_LONG).show();
        }
    }

    private void checkIfThemeExists(final String themeName, final String uid, final String email, final String hash) {
        String tag_string_req = "req_if_them_exists";
        StringRequest strReqCheckIfThemeExist = new StringRequest(Request.Method.GET,
                (AppConfig.URL + "?select=checkiftheme(a,b,c,d)&a="+themeName+"&b="+uid+"&c=" + email+"&d="+hash), new Response.Listener<String>() {

            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(String response) {
                JSONArray jArr;
                JSONObject jObj;
                try {
                    jArr = new JSONArray(response);
                    jObj = jArr.getJSONObject(0);

                    if(!jObj.getBoolean("checkiftheme")) {
                        insertTheme(themeName, uid, email, hash);
                    }
                    else{
                        Toast.makeText(getApplicationContext(),
                                "Selline teema on juba olemas.", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.toString(), Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(strReqCheckIfThemeExist, tag_string_req);
    }

    private void insertTheme(String themeName, final String uid, final String email, final String hash) {
        String tag_string_req = "insert_theme";
        StringRequest strReqInsertTheme = new StringRequest(Request.Method.GET,
                AppConfig.URL + "?select=addtheme(a,b,c,d)&a="+themeName+"&b="+uid+"&c=" + email+"&d="+hash, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                JSONArray jArr;
                JSONObject jObj;
                try {
                    jArr = new JSONArray(response);
                    jObj = jArr.getJSONObject(0);

                    if (!jObj.has("error") && jObj.getBoolean("addtheme")) {
                        addThemeText = findViewById(R.id.addThemeText);
                        addThemeText.setText("");
                        getThemes(uid,email, hash);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Insertion error " + error.getMessage());

                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(strReqInsertTheme, tag_string_req);
    }
    private void deleteTheme(final String id, final String uid, final String email, final String hash) {
        final String tag_string_req = "Delete_theme";
        final StringRequest strReqDelteTheme = new StringRequest(Request.Method.GET,
                AppConfig.URL + "?select=deletetheme(a,b,c,d)&a="+id+"&b="+uid+"&c=" + email+"&d="+hash, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray jArr;
                JSONObject jObj=new JSONObject();
                try {
                    if(response.length()!=0){
                        jArr = new JSONArray(response);
                        jObj = jArr.getJSONObject(0);
                    }

                    if (!jObj.has("error") && jObj.getBoolean("deletetheme")) {
                        themesLayout.removeView(themesLayout.findViewById(Integer.parseInt(id)));
                        getThemes(uid,email,hash);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Delete error " + error.getMessage());

                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(strReqDelteTheme, tag_string_req);

    }
    private void getThemes(final String uid, final String email, final String hash){
        themesLayout.removeAllViews();
        String tag_string_req = "Get all themes";
        JsonArrayRequest strReqThemes = new JsonArrayRequest(
                AppConfig.URL+"?select=getthemes(a,b,c)&a="+uid+"&b="+email+"&c="+hash, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONObject jObj1;
                JSONArray jArr = new JSONArray();
                try {
                    jObj1 = response.getJSONObject(0);
                    jArr = jObj1.getJSONArray("getthemes");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for(int i=0; i < jArr.length(); i++){
                    try {
                        final JSONObject themeObject = jArr.getJSONObject(i);
                        final String idString = themeObject.getString("id");

                        final LinearLayout linearLayout = new LinearLayout(getApplicationContext());
                        linearLayout.setOrientation(HORIZONTAL);
                        linearLayout.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,100));
                        linearLayout.setId(parseInt(idString));
                        TextView newTheme = new TextView(getApplicationContext());
                        newTheme.setText(themeObject.getString("name"));
                        newTheme.setId(parseInt(idString));
                        newTheme.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,1));
                        linearLayout.addView(newTheme);
                        Button openThemeBtn = new Button(getApplicationContext());
                        openThemeBtn.setText(R.string.open_theme_btn);
                        openThemeBtn.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,3));
                        openThemeBtn.setOnClickListener(new View.OnClickListener(){

                            @Override
                            public void onClick(View v) {
                                Intent todoList = new Intent(getApplicationContext(),TodoActivity.class);
                                todoList.putExtra("com.myApp.todo.idString",idString);
                                todoList.putExtra("com.myApp.todo.uid",uid);
                                todoList.putExtra("com.myApp.todo.hashString",hash);
                                todoList.putExtra("com.myApp.todo.emailString",email);
                                startActivity(todoList);
                            }
                        });
                        linearLayout.addView(openThemeBtn);
                        Button themeDeleteBtn = new Button(getApplicationContext());
                        themeDeleteBtn.setText(R.string.delete_btn);
                        themeDeleteBtn.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,3));
                        themeDeleteBtn.setOnClickListener(new View.OnClickListener(){
                            @Override
                            public void onClick(View v) {
                                deleteTheme(idString,uid, email, hash);
                            }
                        });
                        linearLayout.addView(themeDeleteBtn);
                        themesLayout.addView(linearLayout);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(strReqThemes, tag_string_req);
    }
}