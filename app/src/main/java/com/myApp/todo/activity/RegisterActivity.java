package com.myApp.todo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.myApp.todo.R;
import com.myApp.todo.app.AppConfig;
import com.myApp.todo.app.AppController;
import com.myApp.todo.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegisterActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private EditText inputFullName;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        inputFullName = findViewById(R.id.name);
        inputEmail = findViewById(R.id.email);
        inputPassword = findViewById(R.id.password);
        Button btnRegister = findViewById(R.id.btnRegister);
        Button btnLinkToLogin = findViewById(R.id.btnLinkToLoginScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        SessionManager session = new SessionManager(getApplicationContext());


        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(RegisterActivity.this,
                    MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String name = inputFullName.getText().toString().trim();
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {

                    checkForUser(name, email, password);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Link to Login Screen
        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),
                        LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    /**
     * Function to store user in  database will post params(tag, name,
     * email, password) to register url
     * */
    private void checkForUser(final String name, final String email,
                              final String password) {
        // Tag used to cancel the request
        Pattern p = Pattern.compile("^[A-Za-z0-9._%-öäü]+@[A-Za-z0-9-]+[.][A-Za-z]+$");
        Matcher m = p.matcher(email);
        boolean matchFound = m.matches();
        if(!matchFound){
            Toast.makeText(getApplicationContext(),
                    "Palun sisetage korrektne email", Toast.LENGTH_LONG).show();
            return;
        }
        else{
            String tag_string_req = "req_register";
            pDialog.setMessage("Registeerin ...");
            showDialog();
            JSONObject params = new JSONObject();

            try {
                params.put("email", email);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            StringRequest strReq1 = new StringRequest(Request.Method.GET,
                    (AppConfig.URL+"?select=usercheck(a)&a="+email), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    boolean res = true;
                    try {
                        JSONArray responseArray = new JSONArray(response);
                        JSONObject responseObj = responseArray.getJSONObject(0);
                        res = responseObj.getBoolean("usercheck");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(res){
                        Toast.makeText(getApplicationContext(),
                                "Sellise emailiga kasutaja on juba olemas.", Toast.LENGTH_LONG).show();
                        hideDialog();
                    }else{
                        registerUser(name,email,password);
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Register Error: " + error.getMessage());
                    Toast.makeText(getApplicationContext(),
                            error.getMessage(), Toast.LENGTH_LONG).show();
                    hideDialog();
                }
            });
            AppController.getInstance().addToRequestQueue(strReq1, tag_string_req);
        }

    }

    private void registerUser(final String name, final String email,
                              final String password){
        String tag_string_req = "req_register";
        pDialog.setMessage("Registeerin ...");
        showDialog();
        // Teen soola valmis
        String hash = null;
        String saltString;
        byte[] salt = new byte[0];
        try {
            salt = getSalt();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        //Soolan
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(salt);
            //Loon hashi
            byte[] bytes = md.digest(password.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            hash = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        saltString = Arrays.toString(salt);



        StringRequest strReq = new StringRequest(Request.Method.GET,
                (AppConfig.URL+"?select=register(a,b,c,d)&a="+email+"&b="+name+"&c="+hash+"&d="+saltString) , new Response.Listener<String>() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(String response) {
                String regexIsNumber = "^[0-9]*$";
                String res = "";
                try {
                    JSONArray responseArray = new JSONArray(response);
                    JSONObject responseObj = responseArray.getJSONObject(0);
                    res = responseObj.getString("usercheck");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (res.trim().matches(regexIsNumber))
                // User successfully stored in postresql
                {

                    Toast.makeText(getApplicationContext(), "Kasutaja registeeritud, logi sisse", Toast.LENGTH_LONG).show();

                    // Launch login activity
                    Intent intent = new Intent(
                            RegisterActivity.this,
                            LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {

                    // Kui on probleem siis tagastan selle.

                    Toast.makeText(getApplicationContext(),
                            response, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());

                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        //Always use a SecureRandom generator
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        //Create array for salt
        byte[] salt = new byte[16];
        //Get a random salt
        sr.nextBytes(salt);
        //return salt
        return salt;
    }
    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}