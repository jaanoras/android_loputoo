package com.myApp.todo.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.myApp.todo.R;
import com.myApp.todo.app.AppConfig;
import com.myApp.todo.app.AppController;
import com.myApp.todo.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Objects;

import static android.widget.LinearLayout.HORIZONTAL;
import static com.android.volley.VolleyLog.TAG;

public class TodoActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private LinearLayout tasksLayout;
    private EditText addTaskText;
    private TextView addTaskDate;
    private SessionManager session;

    private static final String tag_string_req = "post_article_date";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        tasksLayout = findViewById(R.id.tasksLayout);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Intent intent = getIntent();
        final String email = intent.getStringExtra("com.myApp.todo.emailString");
        final String hash = intent.getStringExtra("com.myApp.todo.hashString");
        final String category = intent.getStringExtra("com.myApp.todo.idString");
        getTasks(category,email,hash);
        Button btnAddTaskDate = findViewById(R.id.btnAddTaskDate);
        Button btnAddTask = findViewById(R.id.btnAddTask);
        addTaskDate = findViewById(R.id.addTaskDate);
        addTaskText = findViewById(R.id.addTaskText);

        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn() || email==null || hash==null || email.isEmpty() || hash.isEmpty()) {
            logoutUser();
        }
        btnAddTaskDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                //newFragment.setArguments();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

        btnAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newDate = addTaskDate.getText().toString().trim();
                String newTask = addTaskText.getText().toString().trim();
                if (!newTask.isEmpty()) {
                    if(!newDate.isEmpty()){
                        String[] myDate= newDate.split("\\.");
                        String temp = myDate[0];
                        myDate[0]=myDate[2];
                        myDate[2]=temp;
                        String databaseDate = TextUtils.join("-",myDate);
                        StringRequest strReq = new StringRequest(Request.Method.GET,
                                AppConfig.URL+ "?select=addtask(a,b,c,d,e)&a="+newTask+"&b="+category+"&c="+databaseDate+"&d="+email+"&e="+hash, new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                JSONArray jArr;
                                JSONObject jObj;
                                try {
                                    jArr = new JSONArray(response);
                                    jObj = jArr.getJSONObject(0);
                                    if(jObj.has("error")){
                                        boolean error = jObj.getBoolean("error");
                                        if (!error) {
                                            addTaskText.setText("");
                                            getTasks(category,email,hash);
                                        }
                                    }
                                    else if(jObj.getBoolean("addtask")){
                                        addTaskText.setText("");
                                        getTasks(category,email,hash);
                                    }
                                    else{
                                        Toast.makeText(getApplicationContext(),
                                                "Authentimine ebaõnnestus, palun proovi uuesti sisse logida.", Toast.LENGTH_LONG)
                                                .show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
                    }else{
                        Toast.makeText(getApplicationContext(),
                                "Palun sisesta ka kuupäev", Toast.LENGTH_LONG)
                                .show();
                    }
                } else {
                    // Prompt user
                    Toast.makeText(getApplicationContext(),
                            "Palun sisesta ülesande nimetus", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String monthString = String.valueOf(month+1);
        if(month<9){
            monthString = "0" + monthString;
        }
        addTaskDate.setText(dayOfMonth + "." + monthString + "." + year);
    }


    private void logoutUser() {
        session.setLogin(false);


        // Launching the login activity
        Intent intent = new Intent(TodoActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
    public static class DatePickerFragment extends DialogFragment {


        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(Objects.requireNonNull(getActivity()), (TodoActivity)getActivity(), year, month, day);
        }
    }

    private void getTasks(final String category, final String email, final String hash){
        tasksLayout.removeAllViews();
        String tag_string_req = "Get tasks";
        if(category != null && !category.isEmpty() && email != null && !email.isEmpty() && hash!=null && !hash.isEmpty()) {
            JsonArrayRequest tasksArr = new JsonArrayRequest(
                    AppConfig.URL + "?select=gettasks(a,b,c)&a=" + category + "&b=" + email + "&c=" + hash, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    JSONArray jArr = new JSONArray();
                    try {
                        jArr = response.getJSONObject(0).getJSONArray("gettasks");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    for (int i = 0; i < Objects.requireNonNull(jArr).length(); i++) {
                        try {
                            final JSONObject taskObject = jArr.getJSONObject(i);
                            final int taskId = taskObject.getInt("id");
                            LinearLayout linearLayout = new LinearLayout(getApplicationContext());
                            linearLayout.setOrientation(HORIZONTAL);
                            linearLayout.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 80));
                            linearLayout.setId(taskId);

                            LinearLayout linearLayout2 = new LinearLayout(getApplicationContext());
                            linearLayout2.setOrientation(HORIZONTAL);
                            linearLayout2.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 120));
                            linearLayout2.setId(taskId + 1000000);

                            LinearLayout linearLayout3 = new LinearLayout(getApplicationContext());
                            linearLayout3.setOrientation(HORIZONTAL);
                            linearLayout3.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 50));
                            linearLayout3.setId(taskId + 2000000);


                            TextView newTask = new TextView(getApplicationContext());
                            newTask.setText(taskObject.getString("name"));
                            newTask.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 4));
                            linearLayout.addView(newTask);

                            TextView isDoneText = new TextView(getApplicationContext());
                            isDoneText.setText(R.string.text_done);
                            isDoneText.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 6));
                            linearLayout.addView(isDoneText);


                            CheckBox isDone = new CheckBox(getApplicationContext());
                            isDone.setChecked(taskObject.getBoolean("done"));
                            isDone.setId(taskId + 9000000);
                            isDone.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 6));
                            isDone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    setChecked(taskId, email, hash);
                                }
                            });
                            linearLayout.addView(isDone);


                            TextView duedate = new TextView(getApplicationContext());
                            duedate.setText(taskObject.getString("duedate"));
                            duedate.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1));
                            linearLayout2.addView(duedate);

                            Button taskDeleteBtn = new Button(getApplicationContext());
                            taskDeleteBtn.setText(R.string.btn_delete);
                            taskDeleteBtn.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 2));
                            taskDeleteBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    deleteTask(taskId, email, hash);
                                }
                            });
                            linearLayout2.addView(taskDeleteBtn);

                            tasksLayout.addView(linearLayout);
                            tasksLayout.addView(linearLayout2);
                            addTaskText.setText("");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }

            });
            AppController.getInstance().addToRequestQueue(tasksArr, tag_string_req);
        }
        else{
            Log.i("getTasks","not enough variables in getTasks");
        }
    }
    private void deleteTask( final int id, String email, String hash) {
        final String tag_string_req = "Delete_task";
        StringRequest strReqDelTask = new StringRequest(Request.Method.POST,
                AppConfig.URL+"?select=deletetask(a,b,c)&a="+id+"&b="+email+"&c="+hash, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray jArr;
                JSONObject jObj;
                try {
                    jArr = new JSONArray(response);
                    jObj = jArr.getJSONObject(0);
                    if(!jObj.getBoolean("deletetask")){
                        Toast.makeText(getApplicationContext(),
                                "Authentimine ebaõnnestus, palun proovi uuesti sisse logida.", Toast.LENGTH_LONG)
                                .show();
                    }

                    else{
                        tasksLayout.removeView(tasksLayout.findViewById(id));
                        tasksLayout.removeView(tasksLayout.findViewById(id+1000000));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Delete error " + error.getMessage());

                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(strReqDelTask, tag_string_req);

    }
    private void setChecked(int taskId, String email, String hash) {
        final String tag_string_req = "Set checked";
        CheckBox isDone = findViewById(taskId+9000000);
        boolean isChecked = isDone.isChecked();
        StringRequest strSetPosted = new StringRequest(Request.Method.GET,
                AppConfig.URL + "?select=setchecked(a,b,c,d)&a="+taskId+"&b="+isChecked+"&c="+email+"&d="+hash, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray jArr;
                JSONObject jObj;
                try {
                    jArr = new JSONArray(response);
                    jObj = jArr.getJSONObject(0);
                    if(!jObj.getBoolean("setchecked")){
                        Toast.makeText(getApplicationContext(),
                                "Authentimine ebaõnnestus, palun proovi uuesti sisse logida.", Toast.LENGTH_LONG)
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "update error " + error.getMessage());

                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        AppController.getInstance().addToRequestQueue(strSetPosted, tag_string_req);
    }
}
